//
//  OWMClient.h
//  Open Weather Map
//
//  Created by Rohan Parolkar on 4/12/16.
//  Copyright © 2016 SimplyTapp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "OWMConstants.h"

//  Protocol for resolving OWMClient requests
@protocol OWMClientDelegate <NSObject>

- (void)OWMResponseWasReceived;
- (void)OWMRequestWasCancelled;

@end

@interface OWMClient : NSObject

//  Delegate to handle OWMClient protocol
@property (nonatomic) id<OWMClientDelegate> delegate;

//  Stored results
@property (nonatomic) NSMutableDictionary *resultsDictionary;

//  Singleton to maintain OWMClient and connection state
+ (id)sharedInstance;

//  Externally accessible methods for initiating and cancelling OWMClient requests
- (void)sendOWMRequestForCity1:(CLLocationCoordinate2D)city1 City2:(CLLocationCoordinate2D)city2;
- (void)cancel;

@end
