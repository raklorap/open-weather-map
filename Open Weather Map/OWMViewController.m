//
//  OWMViewController.m
//  Open Weather Map
//
//  Created by Rohan Parolkar on 4/1/16.
//  Copyright © 2016 SimplyTapp. All rights reserved.
//

#import "OWMViewController.h"
#import "NSString+OWM.h"
#import "OWMClient.h"

//  Enumerator for view controller mode
typedef enum
{
    OWMViewControllerModeInitial,
    OWMViewControllerModeFetching,
    OWMViewControllerModeCancelling,
    OWMViewControllerModeFetched
} OWMViewControllerMode;

@interface OWMViewController ()<UITextFieldDelegate, OWMClientDelegate>
{
    //  Use sets to validate textfield selection
    NSSet *latTextFieldSet;
    NSSet *lonTextFieldSet;
}

//  View properties
@property (weak, nonatomic) IBOutlet UITextField *lat1TextField;
@property (weak, nonatomic) IBOutlet UITextField *lon1TextField;
@property (weak, nonatomic) IBOutlet UITextField *lat2TextField;
@property (weak, nonatomic) IBOutlet UITextField *lon2TextField;

@property (weak, nonatomic) IBOutlet UIButton *resultsButton;
@property (weak, nonatomic) IBOutlet UITextView *resultsTextView;

@property (nonatomic) OWMClient *owmClient;
@property (nonatomic) OWMViewControllerMode mode;
@property (nonatomic, readonly) NSString *resultsText;

@end

@implementation OWMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    latTextFieldSet = [NSSet setWithObjects:self.lat1TextField, self.lat2TextField, nil];
    lonTextFieldSet = [NSSet setWithObjects:self.lon1TextField, self.lon2TextField, nil];
    
    self.owmClient = [OWMClient sharedInstance];
    self.owmClient.delegate = self;
    
    self.mode = OWMViewControllerModeInitial;
    
    //  Add gesture recognizer to dismiss keyboard on background tap
    UITapGestureRecognizer *endEditingTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEditing)];
    [self.view addGestureRecognizer:endEditingTapGestureRecognizer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tappedResultsButton:(UIButton *)sender
{
    //  Act according to view mode
    switch (self.mode) {
        case OWMViewControllerModeFetching:
        {
            self.mode = OWMViewControllerModeCancelling;
            [self cancelOWMRequest];
        }
            break;
        default:
        {
            self.mode = OWMViewControllerModeFetching;
            [self fetchOWMResults];
        }
            break;
    }
}

- (void)fetchOWMResults
{
    CLLocationCoordinate2D city1Coordinates = CLLocationCoordinate2DMake(self.lat1TextField.text.floatValue, self.lon1TextField.text.floatValue);
    CLLocationCoordinate2D city2Coordinates = CLLocationCoordinate2DMake(self.lat2TextField.text.floatValue, self.lon2TextField.text.floatValue);
    [self.owmClient sendOWMRequestForCity1:city1Coordinates City2:city2Coordinates];
}

- (void)cancelOWMRequest
{
    [self.owmClient cancel];
}

- (void)setMode:(OWMViewControllerMode)mode
{
    _mode = mode;
    switch (_mode) {
        case OWMViewControllerModeFetching:
        {
            [self.resultsButton setTitle:BUTTON_TITLE_FETCHING forState:UIControlStateNormal];
            self.resultsButton.userInteractionEnabled = YES;
            self.resultsTextView.text = RESULTS_TEXT_FETCHING;
        }
            break;
        case OWMViewControllerModeCancelling:
        {
            [self.resultsButton setTitle:BUTTON_TITLE_CANCELLING forState:UIControlStateNormal];
            self.resultsButton.userInteractionEnabled = NO;
            self.resultsTextView.text = RESULTS_TEXT_CANCELLING;
        }
            break;
        case OWMViewControllerModeInitial:
        {
            [self.resultsButton setTitle:BUTTON_TITLE_INITIAL forState:UIControlStateNormal];
            self.resultsButton.userInteractionEnabled = YES;
            self.resultsTextView.text = RESULTS_TEXT_INITIAL;
        }
            break;
        default:
        {
            [self.resultsButton setTitle:BUTTON_TITLE_INITIAL forState:UIControlStateNormal];
            self.resultsButton.userInteractionEnabled = YES;
            self.resultsTextView.text = self.resultsText;
        }
            break;
    }
    [self.resultsButton setNeedsLayout];
}

- (NSString *)resultsText
{
    NSDictionary *resultsDictionary = self.owmClient.resultsDictionary;
    
    if ([resultsDictionary objectForKey:CITY1ERRORKEY] || [resultsDictionary objectForKey:CITY2ERRORKEY]) {
        return RESULTS_TEXT_ERROR;
    }
    
    NSString *city1Name = [[resultsDictionary objectForKey:CITY1KEY] objectForKey:NAMEKEY];
    NSString *city2Name = [[resultsDictionary objectForKey:CITY2KEY] objectForKey:NAMEKEY];
    
    NSDictionary *city1Sys = [[resultsDictionary objectForKey:CITY1KEY] objectForKey:SYSKEY];
    NSDictionary *city2Sys = [[resultsDictionary objectForKey:CITY2KEY] objectForKey:SYSKEY];
    
    double sunrise1 = [[city1Sys objectForKey:SUNRISEKEY] doubleValue];
    double sunset1 = [[city1Sys objectForKey:SUNSETKEY] doubleValue];
    double sunrise2 = [[city2Sys objectForKey:SUNRISEKEY] doubleValue];
    double sunset2 = [[city2Sys objectForKey:SUNSETKEY] doubleValue];
    
    NSTimeInterval sunshine1 = sunset1 - sunrise1;
    NSTimeInterval sunshine2 = sunset2 - sunrise2;

    if ([city1Name isEqualToString:city2Name]) {
        return [NSString stringWithFormat:FORMAT_SAME_CITY, [NSString stringFromTimeInterval:sunshine1], city1Name];
    }
    else if (sunshine1 == sunshine2) {
        return [NSString stringWithFormat:FORMAT_SAME_SUNSHINE, city1Name, city2Name];
    }
    else {
        NSTimeInterval diff = sunshine1 - sunshine2;
        NSString *comparator;
        if (diff>0) {
            comparator = GREATER_THAN_STRING;
        }
        else {
            comparator = LESS_THAN_STRING;
        }
        NSString *diffString = [NSString stringFromTimeInterval:fabs(diff)];
        return [NSString stringWithFormat:FORMAT_NORMAL, city1Name, diffString, comparator, city2Name];
    }
}

#pragma mark OWMClientDelegate

//  Reset results button and display response. Set UI updates back on main thread.
- (void)OWMResponseWasReceived
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.mode = OWMViewControllerModeFetched;
    });
}

//  Reset view upon cancellation confirmation. Set UI updates back on main thread.
- (void)OWMRequestWasCancelled
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.mode = OWMViewControllerModeInitial;
    });
}

#pragma mark UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //  First check if input string is valid, given the existing original string in text field
    if ([string isValidReplacementForOriginalString:textField.text])
    {
        //  Then validate that the new string would be a valid latitude or longitude
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if ([latTextFieldSet containsObject:textField])
        {
            return [newString isValidLatitudeString];
        }
        if ([lonTextFieldSet containsObject:textField])
        {
            return [newString isValidLongitudeString];
        }
    }
    return NO;
}

//  Behavior for return button
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

//  End editing if tap gesture recognizer is triggered
- (void)endEditing
{
    [self.view endEditing:YES];
}

@end
