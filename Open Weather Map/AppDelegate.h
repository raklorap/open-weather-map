//
//  AppDelegate.h
//  Open Weather Map
//
//  Created by Rohan Parolkar on 4/1/16.
//  Copyright © 2016 SimplyTapp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

