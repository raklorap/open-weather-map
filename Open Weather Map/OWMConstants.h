//
//  OWMConstants.h
//  Open Weather Map
//
//  Created by Rohan Parolkar on 4/14/16.
//  Copyright © 2016 SimplyTapp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OWMConstants : NSObject

extern NSString * const OWM_API_URL_LAT_LONG;

extern NSString * const CITY1KEY;
extern NSString * const CITY2KEY;
extern NSString * const CITY1ERRORKEY;
extern NSString * const CITY2ERRORKEY;

extern NSString * const NAMEKEY;
extern NSString * const SYSKEY;
extern NSString * const SUNRISEKEY;
extern NSString * const SUNSETKEY;

extern NSString * const FORMAT_SAME_CITY;
extern NSString * const FORMAT_SAME_SUNSHINE;
extern NSString * const FORMAT_NORMAL;

extern NSString * const RESULTS_TEXT_INITIAL;
extern NSString * const RESULTS_TEXT_FETCHING;
extern NSString * const RESULTS_TEXT_CANCELLING;
extern NSString * const RESULTS_TEXT_ERROR;

extern NSString * const BUTTON_TITLE_INITIAL;
extern NSString * const BUTTON_TITLE_FETCHING;
extern NSString * const BUTTON_TITLE_CANCELLING;

extern NSString * const GREATER_THAN_STRING;
extern NSString * const LESS_THAN_STRING;

@end
