//
//  NSString+OWM.m
//  Open Weather Map
//
//  Created by Rohan Parolkar on 4/12/16.
//  Copyright © 2016 SimplyTapp. All rights reserved.
//

#import "NSString+OWM.h"

@implementation NSString (OWM)

- (BOOL)isValidReplacementForOriginalString:(NSString *)originalString
{
    //  If replacement string is a digit, then it will be allowed, unless it is a leading zero
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([self rangeOfCharacterFromSet:notDigits].location == NSNotFound)
    {
        if ([self isEqualToString:@"0"] && originalString.length<1) {
            return NO;
        }
        return YES;
    }
    //  If replacement string is a unique, non-leading period then it will be allowed
    else if ([self isEqualToString:@"."] && originalString.length>0 && [originalString rangeOfString:self].location == NSNotFound) return YES;
    //  If replacement string is a unique leading minus sign then it will be allowed
    else if ([self isEqualToString:@"-"] && originalString.length<1) return YES;
    return NO;
}

- (BOOL)isValidLatitudeString
{
    return fabsf(self.floatValue)<=90.0;
}

- (BOOL)isValidLongitudeString
{
    return fabsf(self.floatValue)<=180.0;
}

+ (NSString *)stringFromTimeInterval:(NSTimeInterval)timeInterval
{
    NSInteger ti = (NSInteger)timeInterval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%ld hours, %ld minutes, and %ld seconds", (long)hours, (long)minutes, (long)seconds];
}

@end
