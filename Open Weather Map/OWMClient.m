//
//  OWMClient.m
//  Open Weather Map
//
//  Created by Rohan Parolkar on 4/12/16.
//  Copyright © 2016 SimplyTapp. All rights reserved.
//

#import "OWMClient.h"

@interface OWMClient()
{
    //  Booleans to track response completion
    BOOL receivedCity1;
    BOOL receivedCity2;
}

@property (nonatomic) NSURLSession *session;

@end

@implementation OWMClient

+ (id)sharedInstance
{
    static OWMClient *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)sendOWMRequestForCity1:(CLLocationCoordinate2D)city1 City2:(CLLocationCoordinate2D)city2
{
    if (!self.session) {
        self.session = [NSURLSession sharedSession];
    }
    
    receivedCity1 = NO;
    receivedCity2 = NO;
    self.resultsDictionary = [NSMutableDictionary new];
    
    NSString *city1URLString = [[NSString stringWithFormat:OWM_API_URL_LAT_LONG, city1.latitude, city1.longitude] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURLSessionDataTask *dataTask1 = [self.session dataTaskWithURL:[NSURL URLWithString:city1URLString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        receivedCity1 = YES;
        [self receivedCity1Response:response data:data error:error];
    }];
    
    NSString *city2URLString = [[NSString stringWithFormat:OWM_API_URL_LAT_LONG, city2.latitude, city2.longitude] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURLSessionDataTask *dataTask2 = [self.session dataTaskWithURL:[NSURL URLWithString:city2URLString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        receivedCity2 = YES;
        [self receivedCity2Response:response data:data error:error];
    }];
    
    [dataTask1 resume];
    [dataTask2 resume];
}

//  Set city and error data accordingly and check for completion before calling protocol methods
- (void)receivedCity1Response:(NSURLResponse *)response data:(NSData *)data error:(NSError *)error
{
    if (error) {
        [self.resultsDictionary setObject:error forKey:CITY1ERRORKEY];
    }
    else {
        NSDictionary* cityResultsDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                                error:&error];
        [self.resultsDictionary setObject:cityResultsDictionary forKey:CITY1KEY];
    }
    
    [self checkForCompletion];
}

- (void)receivedCity2Response:(NSURLResponse *)response data:(NSData *)data error:(NSError *)error
{
    if (error) {
        [self.resultsDictionary setObject:error forKey:CITY2ERRORKEY];
    }
    else {
        NSDictionary* cityResultsDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                                error:&error];
        [self.resultsDictionary setObject:cityResultsDictionary forKey:CITY2KEY];
    }
    
    [self checkForCompletion];
}


- (void)checkForCompletion
{
    //  Call protocol methods when completion has been reached
    if (receivedCity1 && receivedCity2) {
        [self.delegate OWMResponseWasReceived];
    }
}

- (void)cancel
{
    [self.session invalidateAndCancel];
}

@end
