//
//  OWMConstants.m
//  Open Weather Map
//
//  Created by Rohan Parolkar on 4/14/16.
//  Copyright © 2016 SimplyTapp. All rights reserved.
//

#import "OWMConstants.h"

@implementation OWMConstants

NSString * const OWM_API_URL_LAT_LONG = @"http://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&appid=85f1d9c3e21f094ab5f1e7c3878593f5";

NSString * const CITY1KEY = @"city1";
NSString * const CITY2KEY = @"city2";
NSString * const CITY1ERRORKEY = @"city1error";
NSString * const CITY2ERRORKEY = @"city2error";

NSString * const NAMEKEY = @"name";
NSString * const SYSKEY = @"sys";
NSString * const SUNRISEKEY = @"sunrise";
NSString * const SUNSETKEY = @"sunset";

NSString * const FORMAT_SAME_CITY = @"There are %@ of sunshine in the city of %@. Enter pairs of coordinates from different cities and press \"Get Results\" to get a comparison.";
NSString * const FORMAT_SAME_SUNSHINE = @"The cities of %@ and %@ have equal amounts of sunshine.";
NSString * const FORMAT_NORMAL = @"The city of %@ has %@ %@ sunshine than the city of %@.";

NSString * const RESULTS_TEXT_INITIAL = @"Enter latitudes and longitudes to be compared and press \"Get Results\"";
NSString * const RESULTS_TEXT_FETCHING = @"Getting Results. Press \"Cancel\" to stop and do a new search.";
NSString * const RESULTS_TEXT_CANCELLING = @"Cancelling...";
NSString * const RESULTS_TEXT_ERROR = @"There was an error retrieving the data. Please check your internet connection and try again later. If the problem persists, contact Rohan at rohan@rohanparolkar.com for support.";

NSString * const BUTTON_TITLE_INITIAL = @"Get Results";
NSString * const BUTTON_TITLE_FETCHING = @"Cancel";
NSString * const BUTTON_TITLE_CANCELLING = @"Cancelling...";

NSString * const GREATER_THAN_STRING = @"more";
NSString * const LESS_THAN_STRING = @"less";

@end
