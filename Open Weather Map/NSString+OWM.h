//
//  NSString+OWM.h
//  Open Weather Map
//
//  Created by Rohan Parolkar on 4/12/16.
//  Copyright © 2016 SimplyTapp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (OWM)

- (BOOL)isValidReplacementForOriginalString:(NSString *)originalString;

- (BOOL)isValidLatitudeString;

- (BOOL)isValidLongitudeString;

+ (NSString *)stringFromTimeInterval:(NSTimeInterval)timeInterval;

@end
